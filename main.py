from random import randint
import cachetools
import timeit

@cachetools.functools.lru_cache(maxsize=None)
def test_popcnt(v):
    cnt = 0
    while (v):
        cnt += 1
        v &= v - 1
    return cnt

def test_mysort(arr):
    return sorted(arr, key=lambda x: (test_popcnt(x), x))


def create_random_list(len_list: int):
    return [0 * x + randint(0, 9999999999999999999) for x in range(0, len_list)]

def popcnt(number: int):
    b = 18446744073709551616
    counting = 0
    while b > 0:
        z = number & b
        if z == 0: counting += 0
        else: counting += 1
        b = b >> 1
    return counting

def quick_sort_binary(array):
    if len(array) > 1:
        pivot = array.pop()

        grtr_lst, equal_lst, smlr_lst = [], [pivot], []
        for item in array:
            if item[0] == pivot[0]:
                equal_lst.append(item)
            elif item[0] > pivot[0]:
                grtr_lst.append(item)
            else:
                smlr_lst.append(item)
        return quick_sort_binary(smlr_lst) + equal_lst + quick_sort_binary(grtr_lst)
    else:
        return array

def quick_sort_int(array):
    if len(array) > 1:
        pivot = array.pop()

        grtr_lst, equal_lst, smlr_lst = [], [pivot], []
        for item in array:
            if item == pivot:
                equal_lst.append(item)
            elif item > pivot:
                grtr_lst.append(item)
            else:
                smlr_lst.append(item)
        return quick_sort_int(smlr_lst) + equal_lst + quick_sort_int(grtr_lst)
    else:
        return array

def run_sort(array):
    dict_binary = {popcnt(a): [] for a in array}
    for a in array:
        dict_binary[popcnt(a)] += [a]

    array_binary = list(dict_binary.items())
    array_sort = quick_sort_binary(array_binary)

    sort_arr = []
    for i in array_sort:
        sort_arr.append(quick_sort_int(i[1]))

    return sum(sort_arr, [])


arr = create_random_list(100)
print("test_mysort = \n", test_mysort(arr))
print("run_sort = \n", run_sort(arr))
print("\ntest_mysort(arr) == run_sort(arr) -->", test_mysort(arr) == run_sort(arr))

print("time test_mysort = ", timeit.timeit(stmt="test_mysort(arr)", number=1000, globals=globals()))
print("time run_sort = ", timeit.timeit(stmt="run_sort(arr)", number=1000, globals=globals()))